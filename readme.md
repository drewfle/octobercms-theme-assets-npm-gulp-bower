# OctoberCMS Theme Assets - Gulp Project with Bootstrap-Sass, Font Awesome, Angular UI Bootstrap, and AngularJs

OctoberCMS theme assets project built wiht npm, bower, and gulp.

## Usage

### Init Project

```
npm install
gulp
```

### Development

```
gulp watch
```

## Additional Notes

### Referenece

[Getting started with gulp](https://markgoodyear.com/2014/01/getting-started-with-gulp)

### Basic Setup

```
npm init &&
bower init &&
bower install angular angular-bootstrap bootstrap-sass-official fontawesome --save &&
npm install gulp-bower gulp-ruby-sass gulp-notify gulp-autoprefixer gulp-minify-css gulp-jshint gulp-concat gulp-uglify gulp-rename gulp-imagemin gulp-livereload gulp-cache del --save-dev &&
mkdir -p source/app source/img \
source/sass/controls source/sass/layouts \
source/sass/pages source/sass/theme &&
cd source/sass &&
touch themes.scss \
theme/_all.scss theme/_boot.scss theme/_fonts.scss \
theme/_variables.scss theme/_mixins.scss theme/_vendor.scss \
pages/_all.scss \
layouts/_all.scss \
controls/_all.scss && cd ../.. &&
touch gulpfile.js
```

