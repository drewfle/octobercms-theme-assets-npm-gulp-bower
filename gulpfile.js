var del = require('del'),
    gulp = require('gulp'), 
    autoprefixer = require('gulp-autoprefixer'),
    bower = require('gulp-bower'),
    concat = require('gulp-concat'),
    jshint = require('gulp-jshint'),
    minifycss = require('gulp-minify-css'),
    notify = require("gulp-notify"),
    rename = require('gulp-rename'),
    sass = require('gulp-ruby-sass'),
    uglify = require('gulp-uglify')
    cache = require('gulp-cache'),
    imagemin = require('gulp-imagemin'),
    livereload = require('gulp-livereload');
    
var bowerDir = './bower_components';

var config = {
  imagesPath: {
    src: './source/img',
    dest: './images'
  },
  stylesPath: {
    src: './source/sass',
    dest: './css'
  },
  scriptsPath: {
    app: {
      src: './source/app',
      dest: './javascripts/app'
    },
    libs: {
      src: [
        bowerDir + '/angular/angular.js',
        bowerDir + '/angular/angular.min.js',
        bowerDir + '/angular-bootstrap/ui-bootstrap-tpls.js',
        bowerDir + '/angular-bootstrap/ui-bootstrap-tpls.min.js'
      ],
      dest: './javascripts/libs'
    }
  },
  fontsPath: {
    fontawesome: {
      src: bowerDir + '/fontawesome/fonts',
      dest: './fonts/fontawesome'
    }
  }
};

gulp.task('bower', function() { 
  return bower()
    .pipe(gulp.dest(bowerDir)) 
});

gulp.task('copy', function() { 
  gulp.src(config.scriptsPath.libs.src) 
    .pipe(gulp.dest(config.scriptsPath.libs.dest))
  gulp.src(config.fontsPath.fontawesome.src + '/**.*') 
    .pipe(gulp.dest(config.fontsPath.fontawesome.dest))
});

gulp.task('images', function() {
  return gulp.src(config.imagesPath.src + '/**/*')
    .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
    .pipe(gulp.dest(config.imagesPath.dest))
    .pipe(notify({ message: 'Images task complete' }));
});

gulp.task('styles', function() { 
  return sass(config.stylesPath.src + '/theme.scss', { style: 'expanded' })
    .pipe(autoprefixer('last 2 version'))
    .pipe(gulp.dest(config.stylesPath.dest))
    .pipe(rename({suffix: '.min'}))
    .pipe(minifycss())
    .pipe(gulp.dest(config.stylesPath.dest))
    .pipe(notify({ message: 'Styles task complete' }));
});

gulp.task('scripts', function() {
  return gulp.src(config.scriptsPath.app.src + '/**/*.js')
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter('default'))
    .pipe(concat('main.js'))
    .pipe(gulp.dest(config.scriptsPath.app.dest))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest(config.scriptsPath.app.dest))
    .pipe(notify({ message: 'Scripts task complete' }));
});

gulp.task('clean', function(cb) {
  del([
    config.imagesPath.dest,
    config.stylesPath.dest,
    config.scriptsPath.app.dest,
    config.fontsPath.fontawesome.dest,
    ], cb)
});

gulp.task('default', ['clean'], function() {
  gulp.start('bower', 'copy', 'images', 'styles', 'scripts');
});

gulp.task('watch', function() {
  gulp.watch(config.imagesPath.src + '/**/*', ['images']); 
  gulp.watch(config.stylesPath.src + '/**/*.scss', ['styles']); 
  gulp.watch(config.scriptsPath.app.src + '/**/*.js', ['scripts']); 
  livereload.listen();
  gulp.watch(['dist/**']).on('change', livereload.changed);
});
